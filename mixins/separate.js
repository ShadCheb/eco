export default {
  filters: {
    splitPrice: (value) => {
      return String(value || 0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ");
    }
  }
}
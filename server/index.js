// const path = require('path');
// const mongoose = require('mongoose');
// const express = require('express');

// const keys = require('./keys');

// const apiRouters = require('./routers/api.routers');
// const { start } = require('repl');

// const PORT = 3003;
// const app = express();

// mongoose.connect(keys.MONGODB_URL,
//   {
//     useUnifieldTopology: true,
//     useNewUrlParser: true,
//   },
//   function(error) {
//     if (error) return console.log('Error db. ', error);

//     start();
//   }
// )

// app.use(express.static(path.join(__dirname, 'dist')));
// // app.get('/', function(req, res) {
// //   res.sendFile(path.join(__dirname, '/index.html'))
// // });
// app.use('/request', apiRouters);

// async function start() {
//   try {
//     app.listen(PORT, () => {
//       console.log(`Server is running in port ${PORT}`);
//     })
//   } catch (err) {
//     console.log('Error server. ', err);
//   }
// }




const express = require('express');
const { Nuxt, Builder } = require('nuxt');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('../nuxt.config');

mongoose.Schema.Types.Boolean.convertToFalse.add('');
mongoose.connect('mongodb://localhost/api', {
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', error => console.error(error));
db.once('open', () => console.log('Connected to database'));

app.use(bodyParser.json());
app.use(express.json());

const productRoutes = require('../routes/products.routers.js');
app.use('/request/products/brands', productRoutes);

config.dev = process.env.NODE_ENV !== 'production';

async function start() {
  const nuxt = new Nuxt(config);
  const { host, port }  = nuxt.options.server;

  await nuxt.ready();
  if (config.dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  }

  app.use(nuxt.render);

  app.listen(port, host);
  console.ready({
    message: `Server listening on host: ${host} port: ${port}`,
    badge: true,
  })
}


const BrandsModel = require('../models/brands.model');


module.exports.get = function() { // params
  try {
    const brands = BrandsModel.find()
      .sort('name');

    return brands;
  } catch(err) {
    console.log('Error. ', err);
    return {};
  }
}



const TypesModel = require('../models/type.model');
const ColorsModel = require('../models/color.model');
const LabelsModel = require('../models/label.model');
const HeelsModel = require('../models/heel.model');


module.exports.types = async function() {
  try {
    const types = await TypesModel.find();

    return types;
  } catch(err) {
    console.log('Error. ', err);
    return {};
  }
}

module.exports.colors = async function() {
  try {
    const colors = await ColorsModel.find();

    return colors;
  } catch(err) {
    console.log('Error. ', err);
    return {};
  }
}

module.exports.labels = async function(params) {
  try {
    const brandId = params.brandId;

    if (!brandId) return;

    const colors = await LabelsModel.find({ brand: brandId });

    return colors;
  } catch(err) {
    console.log('Error. ', err);
    return {};
  }
}

module.exports.heels = async function() {
  try {
    const heels = await HeelsModel.find();

    return heels;
  } catch(err) {
    console.log('Error. ', err);
    return {};
  }
}


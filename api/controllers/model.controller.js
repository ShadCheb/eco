const ModelsModel = require('../models/models.model');
const BrandsModel = require('../models/brands.model');
const PricesModel = require('../models/price.model');
const TypesModel = require('../models/type.model');
const ColorsModel = require('../models/color.model');

// Временная галерея на каждую модель
const galleryList = [
  '/gallery/23.jpeg',
  '/gallery/24.jpeg',
  '/gallery/25.jpeg',
  '/gallery/26.jpeg',
  '/gallery/27.jpeg',
];

module.exports.get = async function(params) { // params
  try {
    const modelId = params.id;

    if (!modelId) return {};

    const model = await ModelsModel.findById(modelId);
    const brand = await BrandsModel.findById(model.brand);
    const price = await PricesModel.findOne({ model: model._id });
    const result = {
      id: model._id,
      image: model.image,
      imagesList: galleryList,
      price,
      brand_id: brand._id,
      brand: brand.name,
      model: model.model,
    };

    return result;
  } catch(err) {
    console.log('Error. ', err);
    return {};
  }
}


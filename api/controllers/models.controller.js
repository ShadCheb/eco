const ModelsModel = require('../models/models.model');
const BrandsModel = require('../models/brands.model');
const PricesModel = require('../models/price.model');

// // Временная галерея на каждую модель
// const 

module.exports.get = async function(params) { // params
  try {
    const brandBrief = params.brand;

    if (!brandBrief) return {};

    const brand = await BrandsModel.findOne({ brief: brandBrief });
    const models = await ModelsModel.find({ brand: brand._id }).sort('model');
    const result = {
      name:   brand.name,
      icon:   brand.icon,
      brief:  brand.brief,
    };
    const promisesModels = models.map(async model => {
      return new Promise (async(reject) => {
        const prices = await PricesModel.findOne({ model: model._id });
        const price = prices.all || Object.values(prices).reduce((acc, curr) => curr + acc, 0);

        reject({
          _id: model._id,
          model: model.model,
          image: model.image,
          brand: model.brand,
          price
        });
      })
    })

    const modelsNew = await Promise.all(promisesModels);

    result.models = modelsNew;

    return result;
  } catch(err) {
    console.log('Error. ', err);
    return {};
  }
}


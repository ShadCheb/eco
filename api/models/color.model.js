const { Schema, Types, model } = require('mongoose');

const colorSchema = new Schema({
  brief: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  color: {
    type: String,
    required: true,
  },
});


module.exports = model('Color', colorSchema);

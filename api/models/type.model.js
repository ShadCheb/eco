const { Schema, Types, model } = require('mongoose');

const typeSchema = new Schema({
  brief: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  img: {
    type: String,
    required: true,
  },
});


module.exports = model('Type', typeSchema);

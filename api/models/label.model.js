const { Schema, Types, model } = require('mongoose');

const labelSchema = new Schema({
  brief: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  img: {
    type: String,
    required: true,
  },
  img_rug: String,
  price: {
    type: Number,
    required: true,
  },
  brand: { 
    type: Types.ObjectId, 
    ref: 'Brands'
  },
});


module.exports = model('Label', labelSchema);

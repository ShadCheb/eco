const { Schema, Types, model } = require('mongoose');

const brandsSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  brief: {
    type: String,
    required: true,
  },
  icon: {
    type: String,
    required: true,
  },
});

module.exports = model('Brands', brandsSchema);

const { Schema, Types, model } = require('mongoose');

const heelSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  brief: {
    type: String,
    required: true,
  },
  img: {
    type: String,
    required: true,
  },
  img_rug: String,
  price: {
    type: Number,
    required: true,
  }
});

module.exports = model('Heel', heelSchema);

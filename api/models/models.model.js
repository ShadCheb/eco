const { Schema, Types, model } = require('mongoose');

const modelsSchema = new Schema({
  model: {
    type: String,
    required: true,
  },
  image: String,
  imagesList: [ String ],
  brand: { 
    type: Types.ObjectId, 
    ref: 'Brands'
  },
});


module.exports = model('Models', modelsSchema);

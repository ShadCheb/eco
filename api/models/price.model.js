const { Schema, Types, model } = require('mongoose');

const priceSchema = new Schema({
  trunk: Number,
  passenger: Number,
  secondrow: Number,
  thirdrow: Number,
  driver: Number,
  all: Number,
  model: { 
    type: Types.ObjectId, 
    ref: 'Models'
  },
});

module.exports = model('Price', priceSchema);

const mongoose = require('mongoose');
const keys = require('../keys');

mongoose.connect(keys.MONGODB_URL, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

const db = mongoose.connection;

db.on('error', error => console.error('Connection error. ', error));
db.once('open', () => console.log('Connected to database'));

module.exports = db;

const express = require('express');
const db = require('./db');

const app = express();


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// const products = require('./routes/products.routers');

// app.use(products);

module.exports = app;


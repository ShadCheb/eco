const brandsController = require('../controllers/brands.controller')

async function get() {
  const res = await brandsController.get();

  return res;
}


export {
  get,
}
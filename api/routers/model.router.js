const modelController = require('../controllers/model.controller')

async function get(param) {
  const res = await modelController.get(param);

  return res;
}

export {
  get
}
const modelsController = require('../controllers/models.controller')

async function get(param) {
  const res = await modelsController.get(param);

  return res;
}

export {
  get
}
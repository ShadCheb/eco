const modelController = require('../controllers/params.controller')

async function get(param) {
  const res = await modelController.types(param);

  return res;
}

export {
  get,
}



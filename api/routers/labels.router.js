const modelController = require('../controllers/params.controller')

async function get(param) {
  const res = await modelController.labels(param);

  return res;
}

export {
  get,
}



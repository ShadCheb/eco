const modelController = require('../controllers/params.controller')

async function get(param) {
  const res = await modelController.colors(param);

  return res;
}

export {
  get,
}



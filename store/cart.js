// const findMax = (array, field) => {
//   if (!array || array.lenght === 0) return 1
//   return Math.max(...array.map(o => o[field]), 0)
// };

const setLocalStorage = (list) => {
  if (process.browser) {
    const KEY = 'cart';
    if (list) {
      localStorage.setItem(KEY, JSON.stringify(list))
    } else {
      // Пока не нужно
      localStorage.removeItem(KEY);
    }
  }
}


export const state = () => ({
  products: [],
})

export const mutations = {
  ADD_COUNT_BY_PRODUCT (state, { productIdx, count }) {
    const product = { ...state.products[productIdx] };

    product.count += count;
    state.products[productIdx] = product;
    setLocalStorage(state.products);
  },
  // Добавляем товар, если его нет в корзине
  ADD_PRODUCT (state, product) {
    const products = [ ...state.products, product ];
    state.products = products;
    setLocalStorage(products);
  },
  ADD_PRODUCTS (state, products) {
    state.products = products;
  },
  // SET_PRODUCT (state, { productId, data }) {
  //   state.products = [...state.products.filter(prod => prod.id !== productId), data];
  // },
  // Убираем товар
  REMOVE_PRODUCT (state, productId) {
    const products = state.products.filter(product => product.id !== productId);
    state.products = products;
    setLocalStorage(products);
  },
  // Меняем количество товара в корзине
  SET_PRODUCT_QTY (state, { productId, qty }) {
    state.products = [
      ...state.products.filter(prod => prod.productId !== productId),
      { ...state.products.find(prod => prod.productId === productId), qty }
    ]
  },
  // // Присваиваем мета-инфу
  // SET_PRODUCTS_BY_IDS (state, products) {
  //   state.metaProducts = products
  // }
}

export const actions = {
  // async setProductsListByIds ({ commit, state }) {
  //   await sleep(50);
  //   const products = await this.$axios.$get('/mock/products.json');
  //   const productsIds = state.products.map(p => p.productId);
  //   await commit('SET_PRODUCTS_BY_IDS', mock.getProductsByIds(products, productsIds));
  // },
  async addCountByProduct ({ commit, dispatch }, { productIdx, count }) {
    await commit('ADD_COUNT_BY_PRODUCT', { productIdx, count });
  },
  async addProduct ({ commit, dispatch }, data) {
    await commit('ADD_PRODUCT', data);
  },
  async addProducts ({ commit, dispatch }, data) {
    await commit('ADD_PRODUCTS', data);
  },
  async removeProduct ({ commit, dispatch }, productId) {
    await commit('REMOVE_PRODUCT', productId);
  },
  async setProductQuantity ({ commit, dispatch }, { productId, qty }) {
    await commit('SET_PRODUCT_QTY', { productId, qty });
    await dispatch('setProductsListbyIds');
  },
}

// export const getters = {
//   getters: {
//     getProductsInCart: state => {
//       const products = []
//       state.products.map(p => {
//         const metaProduct = state.metaProducts.find(mp => mp.id === p.productId)
//         if (metaProduct) {
//           products.push({ ...p, meta: metaProduct })
//         }
//       })
//       return products.sort((a, b) => a.order - b.order)
//     }
//   }
// }

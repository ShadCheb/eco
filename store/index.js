/* State */
export const state = () => ({
  loading: false,

  brandsList: [],
  currentBrand: {},
  currentProduct: {},
  currentPage: '',
  bredcrumbs: [],

  // :TODO вынести в отдельный store
  // product: {}, // Текущий продукт
  typeList: [], // Список материала ковриков
  colorList: [], // Список вариантов цвета ковриков
  heelList: [], // Список вариантов подпятников
  labelBrandList: [], // Список вариантов шильдиков
})


/* Mutations */
export const mutations = {
  SET_BRANDS_LIST (state, list) {
    state.brandsList = list;
  },
  SET_CURRENT_BRAND (state, data) {
    state.currentBrand = data;
  },
  SET_CURRENT_PRODUCT (state, product) {
    state.currentProduct = product
  },
  SET_CURRENT_PAGE (state, name) {
    state.currentPage = name;
  },
  SET_BREADCRUMBS (state, crumbs) {
    state.bredcrumbs = crumbs;
  },
  RESET_BREADCRUMBS (state) {
    state.bredcrumbs = []
  },

  SET_TYPES (state, list) {
    state.typeList = list
  },
  SET_COLORS (state, list) {
    state.colorList = list
  },
  SET_HEELS (state, list) {
    state.heelList = list
  },
  SET_LABELS_BRAND (state, list) {
    state.labelBrandList = list
  },

  SET_LOADING (state, value) {
    state.loading = value;
  }
}


/* Actions */
export const actions = {
  async setBreadcrumbs ({ commit }, breadcrumbs) {
    const { crumbs, currentPage } = breadcrumbs;

    commit('SET_CURRENT_PAGE', currentPage);
    commit('SET_BREADCRUMBS', crumbs);
  },

  async getDataForBrands ({commit, dispatch }, route) {
    await dispatch('getBrandsList');
    await commit('RESET_BREADCRUMBS');
  },
  async getDataForModels ({ state, dispatch }, route) {
    try {
      const linkArr = route.path.split('/');
      linkArr.splice(-1);
      const link = linkArr.join('/');
      const breadcrumbs = {
        currentPage: '',
        crumbs: [
          {
            link: `${link}`,
            name: 'Каталог',
          }
        ]
      };
      const { brand } = route.params;

      await dispatch('getCurrentModelsByBrand', brand);

      breadcrumbs.currentPage = state.currentBrand.name || '';
      await dispatch('setBreadcrumbs', breadcrumbs);
    } catch (err) {
      console.error('Error getDataForModels. ', err);
      throw new Error('Внутреняя ошибка сервера, сообщите администратору');
    }
  },
 
  async getDataForModel ({ state, dispatch }, route) {
    const { model } = route.params;

    await dispatch('getCurrentModel', model);

    const currentProduct = state.currentProduct;
    const linkBrandArr = route.path.split('/');
    linkBrandArr.splice(-1);
    const linkBrand = linkBrandArr.join('/');

    const breadcrumbs = {
      currentPage: currentProduct.model,
      crumbs: [
        {
          link: `/catalog`,
          name: 'Каталог',
        },
        {
          link: `${linkBrand}`,
          name: currentProduct.brand,
        }
      ]
    };

    await dispatch('setBreadcrumbs', breadcrumbs);
  },

  async getBrandsList ({ commit }) {
    try {
      const brands = await this.$axios.$get('/api/brands/get');
      const brandsResult = brands.success ? brands.result : [];

      await commit('SET_BRANDS_LIST', brandsResult);
      // :TODO Вывести ошибку
    } catch (err) {
      console.error('Error getBrandsList. ', err);
      throw new Error('Внутреняя ошибка сервера, сообщите администратору');
    }
  },

  async getCurrentModelsByBrand ({ commit }, brand) {
    const brandData = await this.$axios.$post('/api/models/get', { brand });
    const brandDataResult = brandData.success ? brandData.result : {};

    await commit('SET_CURRENT_BRAND', brandDataResult);
    // :TODO Вывести ошибку
  },

  async getCurrentModel ({ state, commit, dispatch }, id) {
    commit('SET_LOADING', true);
    // Получем информацию о модели
    const model = await this.$axios.$post('/api/model/get', { id });
    const modelResult = model.success ? model.result : {};

    // :TODO Сделать запросы в Promise All
    if (!state.typeList?.length) {
      const typeList = await this.$axios.$get('/api/types/get');
      const typeListResult = typeList.success ? typeList.result : [];

      await commit('SET_TYPES', typeListResult);
    }
    if (!state.colorList?.length) {
      const colorList = await this.$axios.$get('/api/colors/get');
      const colorListResult = colorList.success ? colorList.result : [];

      await commit('SET_COLORS', colorListResult);
    }
    if (!state.heelList?.length) {
      const heelList = await this.$axios.$get('/api/heels/get');
      const heelListResult = heelList.success ? heelList.result : [];

      await commit('SET_HEELS', heelListResult);
    }

    const labelBrandList = await this.$axios.$post('/api/labels/get', { brandId: model.brand_id });
    const labelBrandListResult = labelBrandList.success ? labelBrandList.result : [];

    await commit('SET_CURRENT_PRODUCT', modelResult);
    await commit('SET_LABELS_BRAND', labelBrandListResult);

    commit('SET_LOADING', false);
  },
}
